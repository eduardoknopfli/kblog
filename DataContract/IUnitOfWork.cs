﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract
{
    interface IUnitOfWork
    {
        bool Begin();
        bool Commit();
        bool Rollback();
    }
}

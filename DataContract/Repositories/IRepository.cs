﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract.Repositories
{
    interface IRepository<T>
    {
        T Get(int id);

        T Add(T t);

        T Update(T t);

        bool Delete(int id);
    }
}

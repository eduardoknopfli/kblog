﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract.Entities
{
    public class BaseEntity
    {
        public int ID { get; set; }
    }
}

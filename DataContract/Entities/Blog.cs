﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract.Entities
{
    public class Blog : BaseEntity
    {
        public string Name { get; set; }
        public User Author { get; set; }
        public DateTime CreatedOn { get; set; }

        public ICollection<BlogEntry> Entries { get; set; }
    }
}

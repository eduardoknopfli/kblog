﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract.Entities
{
    public class BlogEntry : BaseEntity
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public User Author { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }

        public ICollection<Comment> Comments { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract.Entities
{
    public class Comment : BaseEntity
    {
        public string Content { get; set; }
        public User Author { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime UpdatedOn { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataContract.Entities
{
    public class PaginatedEntities : List<BaseEntity>
    {
        public List<BaseEntity> Entities { get; set; }
        public int NumResults { get; set; }
        public int CurrentPage { get; set; }
        public int NumPages { get; set; }
        public int PageSize { get; set; }
    }
}
